# Git training scripts #

Scripts for
<https://slides.com/cintiadelrio/git/>

Demo done with [demomagic](https://github.com/paxtonhare/demo-magic)


To clean the local changes, run `./clean.sh`

You should run `./<demo>/run.sh` from each folder in numerical order from this directory.
Use `./<demo>/run.sh -n` to skip enter on each line.

All scripts rely on SSH to the upstream repositories, with write access.
