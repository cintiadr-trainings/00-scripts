#!/usr/bin/env bash
set -eu

########################
# include the magic
########################
. ./demo-magic.sh


########################
# Configure the options
########################

#
# speed at which to simulate typing. bigger num = faster
#
TYPE_SPEED=30

#
# custom prompt
#
# see http://www.tldp.org/HOWTO/Bash-Prompt-HOWTO/bash-prompt-escape-sequences.html for escape sequences
#
DEMO_PROMPT="${GREEN}➜ ${CYAN}\w "

DATETIME=$(date +%Y%m%d_%H%M%S)

cd my-awesome-project
git checkout master
git reset --hard origin/master

# hide the evidence
clear
