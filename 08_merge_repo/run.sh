#!/usr/bin/env bash

set -eu

. ./setenv.sh

#########
p "${PURPLE}[Info] Add extra remote ${COLOR_RESET}"

pe "git remote add boring-proj git@bitbucket.org:cintiadr-trainings/04-boring-project.git"
pe "git fetch boring-proj"
pe "git remote -v"

p "${PURPLE}[Info] Create branch with master ${COLOR_RESET}"
pe "git checkout -b 08_boring_proj_orig"
pe "git reset --hard boring-proj/master"
pe "git push"

p "${PURPLE}[Info] Merge two branches ${COLOR_RESET}"
pe "git checkout -b 08_boring_proj_merge"
pe "git fetch"
pe "git reset --hard origin/master"
pe "git merge 08_boring_proj_orig --allow-unrelated-histories --no-edit"
pe "git push"


git remote remove boring-proj
