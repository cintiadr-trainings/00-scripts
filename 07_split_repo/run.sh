#!/usr/bin/env bash

set -eu

. ./setenv.sh

#########
p "${PURPLE}[Info] Create branch with history of important-folder ${COLOR_RESET}"
pe "git subtree split --prefix=important-folder -b 07_important_subtree"

p "${PURPLE}[Info] Push to repo ${COLOR_RESET}"
pe "git co 07_important_subtree"
pe "git remote add upstream git@bitbucket.org:cintiadr-trainings/03-awesome-subset.git"
pe "git push -f upstream 07_important_subtree:master"

git remote remove upstream
