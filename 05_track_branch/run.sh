#!/usr/bin/env bash

set -eu

. ./setenv.sh

#########

p "${PURPLE}[Info] create new local branch${COLOR_RESET}"
pe "git checkout -b 05_track_branch"
pe "git log"
pe "git remote -v"

p "${PURPLE}[Info] create commit in branch local ${COLOR_RESET}"
pe "echo 05 - track branch ${DATETIME} >> README.md"
pe "git add ."
pe "git commit -m \"05 - Track branch ${DATETIME}\""
pe "git push origin 05_track_branch:05_track_branch_orig"
pe "git log"
