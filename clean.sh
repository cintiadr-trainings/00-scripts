#!/bin/bash -eux

rm -rf my-awesome-project
git clone git@bitbucket.org:cintiadr-trainings/01-awesome-project.git my-awesome-project
(
  cd my-awesome-project
  git reset --hard 0057b1b
  git tag | xargs -n1 git push --delete origin
  git branch -a | fgrep remote | fgrep -v master | fgrep -v secret | cut -d/ -f3 | xargs -n1 git push --delete origin
  git push -f
)

rm -rf my-awesome-project-fork
git clone git@bitbucket.org:cintiadr-trainings/02-awesome-project-fork.git my-awesome-project-fork
(
  cd my-awesome-project-fork
  git reset --hard ff97e4a
  git tag | xargs -n1 git push --delete origin
  git branch -a | fgrep remote | fgrep -v master | cut -d/ -f3 | xargs -n1 git push --delete origin
  git push -f
)
rm -rf my-awesome-project-fork


(
  cd 00_bare
  rm -rf my-awesome-*
)

rm -rf awesome-subset
mkdir awesome-subset
(
  cd awesome-subset
  git init
  echo "Initial commit" > README.md
  git add .
  git commit -m "Silly commit"
  git remote add origin git@bitbucket.org:cintiadr-trainings/03-awesome-subset.git
  git push -f
)
rm -rf awesome-subset
