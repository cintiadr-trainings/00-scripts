#!/usr/bin/env bash

set -eu

. ./setenv.sh

#########

p "${PURPLE}[Info] checkout specific commit ${COLOR_RESET}"
pe "git checkout ff97e4a"
pe "git log"


p "${PURPLE}[Info] commit to detached head ${COLOR_RESET}"
pe "echo 03 - Detached ${DATETIME} >> README.md"
pe "git add ."
pe "git commit -m \"03 - Adding detached ${DATETIME}\""

p "${PURPLE}[Info] adding tag to current commit ${COLOR_RESET}"
pe "git tag 03_detached"
p "${PURPLE}[Info] Pushing tags to origin ${COLOR_RESET}"
pe "git push --tags"
