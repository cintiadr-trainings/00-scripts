#!/usr/bin/env bash

set -eu

. ./setenv.sh

#########

p "${PURPLE}[Info] Changing a file ${COLOR_RESET}"
pe "echo 01 - Mouse mouse ${DATETIME} >> README.md"
pe "git add ."
pe "git commit --author=\"Mickey Mouse <mickey@disney.com>\" -m \"01 - Adding random commit ${DATETIME}\""

p "${PURPLE}[Info] Pushing commit to master ${COLOR_RESET}"
pe "git push"
