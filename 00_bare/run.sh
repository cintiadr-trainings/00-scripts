#!/usr/bin/env bash
set -eu

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd ${DIR}
. ./setenv.sh


pe "mkdir my-awesome-upstream"
pe "cd my-awesome-upstream"

p "${PURPLE}[INFO] Create a new Bare repository${COLOR_RESET}"
pe "git init --bare"

pe "cd .."
p "${PURPLE}[INFO] Clone that repository (#1)${COLOR_RESET}"
pe "git clone ${DIR}/my-awesome-upstream my-awesome-clone-1"

p "${PURPLE}[INFO] Make some change to clone-1${COLOR_RESET}"
pe "cd my-awesome-clone-1"
pe "echo "Test" >> README.md"
pe "git status"
pe "git add ."
pe "git commit -m 'Adding some silly tests'"

p "${PURPLE}[INFO] And push${COLOR_RESET}"
pe "git push"

p "${PURPLE}[INFO] Clone again that repo Clone-2 (#2)${COLOR_RESET}"
pe "cd .."
pe "git clone ${DIR}/my-awesome-upstream my-awesome-clone-2"
pe "cd my-awesome-clone-2"
pe "ls"
pe "git status"
pe "git pull"
pe "ls"
pe "git status"
pe "git log"
