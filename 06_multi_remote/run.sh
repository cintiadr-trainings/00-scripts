#!/usr/bin/env bash

set -eu

. ./setenv.sh

#########

p "${PURPLE}[Info] create new local branch${COLOR_RESET}"
pe "git checkout -b 06_multi_remote"
pe "git log"

p "${PURPLE}[Info] it has only origin remote by default${COLOR_RESET}"
pe "git remote -v"

p "${PURPLE}[Info] but we can add more remotes${COLOR_RESET}"
pe "git remote add upstream git@bitbucket.org:cintiadr-trainings/02-awesome-project-fork.git"
pe "git fetch upstream"

p "${PURPLE}[Info] We can change local branches to any commit hash${COLOR_RESET}"
p "${PURPLE}[Info] Remember: the branch is just an alias to a commit hash${COLOR_RESET}"
pe "git checkout 06_multi_remote"
pe "git log"
pe "git branch -a"
pe "git reset --hard upstream/master"
pe "git log"

p "${PURPLE}[Info] Push other_branch to upstream AND origin${COLOR_RESET}"
pe "git checkout 06_multi_remote"
pe "git push upstream 06_multi_remote:06_multi_remote_upstream"
pe "git push origin 06_multi_remote:06_multi_remote_origin"
pe "git log"


git remote remove upstream
