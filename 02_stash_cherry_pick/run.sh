#!/usr/bin/env bash

set -eu

. ./setenv.sh

#########

p "${PURPLE}[Info] Change two files ${COLOR_RESET}"
pe "git checkout -b 02_cherry_pick"
pe "echo 02 - Some silly change ${DATETIME} >> README.md"
pe "echo 02 - Another thingy >> important-folder/lorem-ipsum.txt"
p "${PURPLE}[Info] Stash some changes ${COLOR_RESET}"
pe "git stash -p"
pe "git stash list"

p "${PURPLE}[Info] Commit the rest of the changes ${COLOR_RESET}"
pe "git status"
pe "git add ."
pe "git commit -m \"02 - Commit 1\""
pe "git push"

p "${PURPLE}[Info] Stash some changes ${COLOR_RESET}"
pe "git stash apply"
pe "git add ."
pe "git commit -m \"02 - Stashed Commit 2\""
pe "git push"


p "${PURPLE}[Info] Cherry pick commit from another branch ${COLOR_RESET}"
pe "git cherry-pick fa17653"
pe "git push"


p "${PURPLE}[Info] Reverting commit ${COLOR_RESET}"
pe "git revert HEAD~2 --no-edit"
pe "git push"
