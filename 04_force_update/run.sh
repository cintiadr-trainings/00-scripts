#!/usr/bin/env bash

set -eu

. ./setenv.sh

#########
p "${PURPLE}[Info] Create some commit and push ${COLOR_RESET}"
pe "git checkout -b 04_force_update_branch"
pe "echo 04 - Force update ${DATETIME} >> README.md"
pe "git add ."
pe "git commit -m \"04 - Force update commit 1 ${DATETIME}\""
pe "git push origin"


p "${PURPLE}[Info] Recreate latest commit, and and push it to upstream only${COLOR_RESET}"
pe "git reset --soft HEAD~1"
pe "git diff --cached"
pe "git add ."
pe "git commit -m \"04 - Force update commit 2 ${DATETIME}\""

p "${PURPLE}[Err] It will fail, as you have to rewrite the history of the branch upstream${COLOR_RESET}"
pe "git push origin || true"

p "${PURPLE}[Info] But you can force (and lose commits)${COLOR_RESET}"
pe "git push origin -f"
pe "git log"
